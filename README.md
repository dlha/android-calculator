# Calculator Android Application

- Implement using [Shunting-Yard Algorithm](https://en.wikipedia.org/wiki/Shunting-yard_algorithm)

- Screenshot:  

[<img src="./screenshot/lightTheme.png" width="300">](LightTheme.png) [<img src="./screenshot/darkTheme.png" width="300">](DarkTheme.png)