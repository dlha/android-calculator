package com.dlha.calculator.utils

import android.util.Log
import java.math.BigDecimal
import java.math.MathContext
import java.util.*

/*
*  Implement Shunting-yard algorithm from https://en.wikipedia.org/wiki/Shunting-yard_algorithm
* */
object ExpressionParser {
//    nested enum
    enum class Operator(val token : String){
        SUM("+"),
        SUB("-"),
        MULTI("*"),
        DIVIDE("÷"),
        PARENTHESES_OPEN("("),
        PARENTHESES_CLOSE(")");

        companion object {
            fun fromToken(token: String): Operator {
                for(operator in values()){
                    if(operator.token == token){
                        return operator
                    }
                }

                throw IllegalArgumentException("Do not support this token")
            }
        }

        fun precedence(): Int{
            return when(this){
                PARENTHESES_OPEN -> 1

                SUM, SUB -> 2

                MULTI, DIVIDE -> 3

                else -> -1 //PARENTHESE_CLOSE
            }
        }
    }

    private fun isNumberic(string : String) : Boolean{
        return try{
            string.toDouble()
            true
        } catch (e: NumberFormatException){
            false
        }
    }

    private fun isOperator(string : String) : Boolean{
        return try {
            Operator.fromToken(string)
            true
        } catch (e: java.lang.IllegalArgumentException){
            false
        }
    }

    fun evaluate(tokens : List<String>) : Double {
        val rpn = infixToRpn(tokens)
        return rpnToDouble(rpn)
    }

    private fun infixToRpn(inputTokens: List<String>): List<String> {
        var tokens = inputTokens
        val output = arrayListOf<String>()
        val stackOperator = Stack<Operator>()

        if(isOperator(tokens.get(0))){
            val op = Operator.fromToken(tokens.get(0))
            when(op){
                Operator.SUB -> {
                    output.add("-"+tokens.get(1))
                    tokens = tokens.drop(2)

                    Log.d("Token", tokens.joinToString())
                }

                Operator.SUM -> {
                    output.add(tokens.get(1))
                    tokens = tokens.drop(2)
                }

                Operator.PARENTHESES_CLOSE -> throw java.lang.IllegalArgumentException("Expession not suppport")
            }

        }

        tokens.forEach {
            when{
                //If it is Numberic
                isNumberic(it) -> output.add(it)

                //If it is Operator
                isOperator(it) -> {
                    val operator = Operator.fromToken(it)
                    var topOperator = if(stackOperator.isNotEmpty()) stackOperator.peek() else null

                    when(operator){
                        Operator.PARENTHESES_CLOSE -> {
                            while(topOperator != null && topOperator != Operator.PARENTHESES_OPEN){
                                output.add(topOperator.token)
                                stackOperator.pop()
                                topOperator = if(stackOperator.isNotEmpty()) stackOperator.peek() else null
                            }

                            if(!stackOperator.isEmpty()) {
                                stackOperator.pop()
                            }
                        }

                        Operator.PARENTHESES_OPEN -> stackOperator.push(operator)

                        else -> {
                            while(topOperator != null && topOperator.precedence() > operator.precedence()){
                                output.add(topOperator.token)
                                stackOperator.pop()
                                topOperator = if (stackOperator.isNotEmpty()) stackOperator.peek() else null
                            }

                            stackOperator.push(operator)
                        }
                    }
                }

                else -> throw IllegalArgumentException("Expession not suppport")
            }
        }

        while(stackOperator.isNotEmpty()){
            output.add(stackOperator.pop().token)
        }

        return output

    }

    private fun rpnToDouble(tokens: List<String>): Double {
        try {
            val stack = Stack<String>()
            Log.d("Token 2", tokens.joinToString())

            tokens.forEach {
                if(isNumberic(it)){
                    stack.push(it)
                } else {

                    val B = stack.pop().toBigDecimal()
                    val A = stack.pop().toBigDecimal()

                    val result = when (Operator.fromToken(it)){
                        Operator.SUM -> A.plus(B)

                        Operator.SUB -> A.minus(B)

                        Operator.MULTI -> A.times(B)

                        Operator.DIVIDE -> {
                            if(B.compareTo(BigDecimal.ZERO) == 0) {
                                throw ArithmeticException()
                            } else
                            A.divide(B, MathContext.DECIMAL128)
                        }

                        else -> throw NumberFormatException()
                    }

                    stack.push(result.toString())
                }
            }

            return stack.pop().toDouble()

        } catch (e: NumberFormatException) {
            throw IllegalArgumentException("Expression is not valid")
        } catch (e: EmptyStackException) {
            throw IllegalArgumentException("Expression is not valid")
        } catch (e: ArithmeticException){
            throw ArithmeticException("Error, cannot divide by zero!")
        }
    }


}