package com.dlha.calculator

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.dlha.calculator.databinding.ActivityMainBinding
import com.dlha.calculator.utils.ExpressionParser
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {
    companion object {
        private const val REGEX_ARITHMETIC_EXPRESSION = "^[0-9+*-÷()., ]+$"
        private const val REGEX_SPLIT_NUMBER = "(?=[-+÷*()])|(?<=[-+÷*()])"
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initButton()

    }

    //Set listener on each button
    private fun initButton() {
        //Number buttons
        binding.btn0.setOnClickListener { setInput(getString(R.string.num0)) }
        binding.btn1.setOnClickListener { setInput(getString(R.string.num1)) }
        binding.btn2.setOnClickListener { setInput(getString(R.string.num2)) }
        binding.btn3.setOnClickListener { setInput(getString(R.string.num3)) }
        binding.btn4.setOnClickListener { setInput(getString(R.string.num4)) }
        binding.btn5.setOnClickListener { setInput(getString(R.string.num5)) }
        binding.btn6.setOnClickListener { setInput(getString(R.string.num6)) }
        binding.btn7.setOnClickListener { setInput(getString(R.string.num7)) }
        binding.btn8.setOnClickListener { setInput(getString(R.string.num8)) }
        binding.btn9.setOnClickListener { setInput(getString(R.string.num9)) }
        binding.btnDot.setOnClickListener { setInput(getString(R.string.dot)) }

        //Operator buttons
        binding.btnSum.setOnClickListener { setInput(getString(R.string.sum)) }
        binding.btnSub.setOnClickListener { setInput(getString(R.string.sub)) }
        binding.btnMul.setOnClickListener { setInput(getString(R.string.multiple)) }
        binding.btnDiv.setOnClickListener { setInput(getString(R.string.div)) }
        binding.btnParentheseOpen.setOnClickListener { setInput(getString(R.string.parentheseOpen)) }
        binding.btnParentheseClose.setOnClickListener { setInput(getString(R.string.parentheseClose)) }

        //Action buttons
        binding.btnClear.setOnClickListener { binding.textViewInput.text = "" }
        binding.btnDel.setOnClickListener { dropLastChar() }
        binding.btnEqual.setOnClickListener { onResult(binding.textViewInput.text.toString()) }
    }

    private fun dropLastChar() {
        val textView = binding.textViewInput
        if (textView.text == null) return

        textView.text = textView.text.dropLast(1)
    }

    private fun setInput(text: String) {
        binding.textViewInput.append(text)
    }

    fun onResult(s: String): Unit {
        val input = s?.trim()?.replace(" ", "")
        try {
            if (validateInput(input)) {
                computeWithShuntingYardAlgorithm(input!!)
                onClear()
            }
        } catch (e: Exception) {
            Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    //Clear Text (Input and Result)
    fun onClear() {
        binding.textViewInput.text = ""
    }

    //Validate Input is Arithmetic or not
    private fun validateInput(expession: String): Boolean {
        return if (expession?.isBlank()) {
            false
        } else {
//          validate that no alphabet character on input
            val pattern = Pattern.compile(REGEX_ARITHMETIC_EXPRESSION)
            pattern.matcher(expession).matches()
        }
    }

    //Using Shunting-Yard Algorithm to compute arithmetic
    private fun computeWithShuntingYardAlgorithm(expression: String) {
        val pattern = Pattern.compile(REGEX_SPLIT_NUMBER)
        val subExpression = expression.split(pattern)
        Log.i("Sub string", "computeWithShuntingYardAlgorithm: " + subExpression)
        val input = arrayListOf<String>()

        subExpression.forEach {
            if (it.isNotEmpty()) {
                input.add(it)
            }
        }

        try {
            val result = ExpressionParser.evaluate(input)
            binding.textViewResult.text =
                if (result == Math.floor(result) && !result.isInfinite()) {
                    "= " + result.toInt().toString()
                } else "= " + result.toString()
        } catch (e: IllegalArgumentException) {
            Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
        } catch (e: ArithmeticException) {
            Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }
}